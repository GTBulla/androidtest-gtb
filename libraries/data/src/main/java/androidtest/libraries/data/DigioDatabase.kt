package androidtest.libraries.data

import android.content.Context
import android.util.Log
import androidtest.libraries.data.dao.CashDao
import androidtest.libraries.data.dao.ProductDao
import androidtest.libraries.data.dao.SpotlightDao
import androidtest.libraries.data.entity.CashEntity
import androidtest.libraries.data.entity.ProductEntity
import androidtest.libraries.data.entity.SpotlightEntity
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Database(
    entities = [
        CashEntity::class,
        SpotlightEntity::class,
        ProductEntity::class,
    ], version = 3, exportSchema = true
)
abstract class DigioDatabase : RoomDatabase() {
    abstract val cashDao: CashDao
    abstract val spotlightDao: SpotlightDao
    abstract val productDao: ProductDao
}

private lateinit var INSTANCE: DigioDatabase

fun getDatabase(context: Context): DigioDatabase {
    synchronized(DigioDatabase::class.java) {
        if (!::INSTANCE.isInitialized) {
            INSTANCE = Room.databaseBuilder(
                context.applicationContext,
                DigioDatabase::class.java,
                "digio_database.db"
            ).fallbackToDestructiveMigration().build()
        }
    }
    return INSTANCE
}

fun DigioDatabase.clearDatabase() {
    CoroutineScope(Dispatchers.IO).launch {
        try {
            cashDao.deleteAll()
            spotlightDao.deleteAll()
            productDao.deleteAll()
        } catch (ex: Exception) {
            Log.e("Digio-Error", ex.toString())
        }
    }
}