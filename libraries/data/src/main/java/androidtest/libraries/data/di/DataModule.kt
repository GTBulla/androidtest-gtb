package androidtest.libraries.data.di

import androidtest.libraries.data.DigioDatabase
import androidtest.libraries.data.getDatabase
import androidtest.libraries.data.getRetrofit
import androidtest.libraries.data.service.DigioService
import org.koin.dsl.module
import retrofit2.Retrofit

val dataModule = module {

    //Database
    factory { getDatabase(context = get()) }

    //Dao
    factory { get<DigioDatabase>().cashDao }
    factory { get<DigioDatabase>().spotlightDao }
    factory { get<DigioDatabase>().productDao }

    //Retrofit
    factory { getRetrofit() }

    //Service
    factory { get<Retrofit>().create(DigioService::class.java) }

}