package androidtest.libraries.data.service

import androidtest.libraries.data.response.DigioProductResponse
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers

interface DigioService {
    @Headers("Content-Type: application/json")
    @GET("sandbox/products")
    fun getProducts(): Call<DigioProductResponse>
}