package androidtest.libraries.data.dao

import androidtest.libraries.data.entity.CashEntity
import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface CashDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(cash: CashEntity)

    @Transaction
    suspend fun clearAndInsert(cash: CashEntity) {
        deleteAll()
        insert(cash)
    }

    @Query(
        """
    SELECT * FROM cash
    """
    )
    fun getList(): LiveData<CashEntity?>

    @Query(
        """
    DELETE FROM cash
    """
    )
    fun deleteAll()

}