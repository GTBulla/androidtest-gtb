package androidtest.libraries.data.response

import androidtest.libraries.data.model.Cash
import androidtest.libraries.data.model.Product
import androidtest.libraries.data.model.Spotlight
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DigioProductResponse(
    val cash: Cash,
    val products: List<Product>,
    @SerializedName("spotlight")
    val spotlights: List<Spotlight>
) : Serializable
