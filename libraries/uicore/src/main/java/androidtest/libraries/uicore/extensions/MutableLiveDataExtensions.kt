package androidtest.libraries.uicore.extensions

import androidx.lifecycle.MutableLiveData

fun <T : Any> MutableLiveData<androidtest.libraries.uicore.utils.Result<T>>.resetStatus() =
    this.postValue(androidtest.libraries.uicore.utils.Result.None)