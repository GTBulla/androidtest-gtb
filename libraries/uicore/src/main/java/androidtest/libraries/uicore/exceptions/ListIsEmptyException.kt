package androidtest.libraries.uicore.exceptions

import androidtest.libraries.uicore.R
import com.blankj.utilcode.util.StringUtils

class ListIsEmptyException : Exception() {
    override val message: String?
        get() = StringUtils.getString(R.string.message_list_is_empty)
}