package androidtest.features.cash.data.irepostory

import androidtest.libraries.data.model.Cash
import androidtest.libraries.uicore.utils.Result
import androidx.lifecycle.LiveData

internal interface ICashRepository {

    enum class CashRepositoryStatus {
        GetCashFromServer,
    }

    val statusGetCashFromServer: LiveData<Result<Cash>>

    suspend fun getCash()
    suspend fun getCashFromServer()
    suspend fun createCashToDb(cash: Cash)
    fun getCashFromDb(): LiveData<Cash?>
    fun resetStatus(status: CashRepositoryStatus)
}