package androidtest.features.cash.data.repository

import androidtest.features.cash.data.irepostory.ICashRepository
import androidtest.libraries.data.dao.CashDao
import androidtest.libraries.data.entity.asDomainModel
import androidtest.libraries.data.model.Cash
import androidtest.libraries.data.model.asEntity
import androidtest.libraries.data.response.DigioProductResponse
import androidtest.libraries.data.service.DigioService
import androidtest.libraries.uicore.extensions.resetStatus
import androidtest.libraries.uicore.utils.Result
import androidtest.libraries.uicore.utils.responseError
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidtest.libraries.uicore.exceptions.ResponseNoBodyException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

internal class CashRepository(
    val dao: CashDao,
    val service: DigioService,
) : ICashRepository {

    private val _statusGetCashFromServer =
        MutableLiveData<Result<Cash>>()
    override val statusGetCashFromServer: LiveData<Result<Cash>>
        get() = _statusGetCashFromServer

    override suspend fun getCash() {
        withContext(Dispatchers.IO) {
            val cash = getCashFromDb()
            if (cash.value == null)
                getCashFromServer()
        }
    }

    override suspend fun getCashFromServer() {
        withContext(Dispatchers.IO) {
            _statusGetCashFromServer.postValue(Result.InProgress)
            service.getProducts().enqueue(object :
                Callback<DigioProductResponse> {
                override fun onResponse(
                    call: Call<DigioProductResponse>,
                    response: Response<DigioProductResponse>
                ) {
                    try {
                        if (response.isSuccessful) {
                            response.body()?.let {
                                _statusGetCashFromServer.postValue(
                                    Result.Success(it.cash)
                                )
                            } ?: throw ResponseNoBodyException()
                        } else {
                            _statusGetCashFromServer.postValue(
                                responseError(response.code())
                            )
                        }
                    } catch (ex: Exception) {
                        _statusGetCashFromServer.postValue(
                            Result.Error(ex, null)
                        )
                    }
                }

                override fun onFailure(call: Call<DigioProductResponse>, t: Throwable) {
                    _statusGetCashFromServer.postValue(
                        Result.Error(t, null)
                    )
                }
            })
        }
    }

    override suspend fun createCashToDb(cash: Cash) {
        withContext(Dispatchers.IO) {
            try {
                dao.clearAndInsert(cash.asEntity())
            } catch (ex: Exception) {
                ex.toString()
            }
        }
    }

    override fun getCashFromDb(): LiveData<Cash?> {
        return Transformations.map(dao.getList()) {
            it?.asDomainModel()
        }
    }

    override fun resetStatus(status: ICashRepository.CashRepositoryStatus) =
        when (status) {
            ICashRepository.CashRepositoryStatus.GetCashFromServer -> _statusGetCashFromServer.resetStatus()
        }

}