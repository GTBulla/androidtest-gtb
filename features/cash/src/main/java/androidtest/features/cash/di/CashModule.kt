package androidtest.features.cash.di

import androidtest.features.cash.data.irepostory.ICashRepository
import androidtest.features.cash.data.repository.CashRepository
import androidtest.features.cash.presenter.viewmodel.CashViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val cashModule = module {

    //Repository
    factory<ICashRepository> {
        CashRepository(
            dao = get(),
            service = get()
        )
    }

    //ViewModel
    viewModel { CashViewModel(repository = get()) }

}