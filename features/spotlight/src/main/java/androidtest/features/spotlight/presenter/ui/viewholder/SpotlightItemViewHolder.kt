package androidtest.features.spotlight.presenter.ui.viewholder

import androidtest.features.spotlight.R
import androidtest.features.spotlight.databinding.SpotlightItemBinding
import androidtest.libraries.data.model.Spotlight
import androidtest.libraries.uicore.extensions.gone
import androidtest.libraries.uicore.extensions.show
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

internal class SpotlightItemViewHolder constructor(
    private val binding: SpotlightItemBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: Spotlight) {
        with(binding) {
            imgMainItem.contentDescription = item.name
            progressImage.show()

            Picasso.get()
                .load(item.bannerURL)
                .error(R.drawable.ic_alert_circle)
                .into(imgMainItem, object : Callback {
                    override fun onSuccess() {
                        progressImage.gone()
                    }

                    override fun onError(e: Exception?) {
                        progressImage.gone()
                    }
                })
        }
    }
}
