package androidtest.features.spotlight.di

import androidtest.features.spotlight.data.irepostory.ISpotlightRepository
import androidtest.features.spotlight.data.repository.SpotlightRepository
import androidtest.features.spotlight.presenter.viewmodel.SpotlightViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val spotlightModule = module {

    //Repository
    factory<ISpotlightRepository> {
        SpotlightRepository(
            dao = get(),
            service = get()
        )
    }

    //ViewModel
    viewModel { SpotlightViewModel(repository = get()) }

}