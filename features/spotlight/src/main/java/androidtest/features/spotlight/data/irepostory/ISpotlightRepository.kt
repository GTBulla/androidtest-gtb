package androidtest.features.spotlight.data.irepostory

import androidtest.libraries.data.model.Spotlight
import androidtest.libraries.uicore.utils.Result
import androidx.lifecycle.LiveData

interface ISpotlightRepository {

    enum class SpotlightRepositoryStatus {
        GetListFromServer,
    }

    val statusGetListFromServer: LiveData<Result<List<Spotlight>>>

    suspend fun getList()
    suspend fun getListFromServer()
    suspend fun createListToDb(list: List<Spotlight>)
    fun getListFromDb(): LiveData<List<Spotlight>?>
    fun resetStatus(status: SpotlightRepositoryStatus)
}