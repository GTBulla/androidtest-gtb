package androidtest.features.product.presenter.ui.viewholder

import androidtest.features.product.R
import androidtest.features.product.databinding.ProductItemBinding
import androidtest.libraries.data.model.Product
import androidtest.libraries.uicore.extensions.gone
import androidtest.libraries.uicore.extensions.show
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

internal class ProductItemViewHolder constructor(
    private val binding: ProductItemBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: Product) {
        with(binding) {
            imgMainItem.contentDescription = item.name
            progressImage.show()

            Picasso.get()
                .load(item.imageURL)
                .error(R.drawable.ic_alert_circle)
                .into(imgMainItem, object : Callback {
                    override fun onSuccess() {
                        progressImage.gone()
                    }

                    override fun onError(e: Exception?) {
                        progressImage.gone()
                    }
                })
        }
    }
}
