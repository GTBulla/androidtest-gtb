package androidtest.features.product.di

import androidtest.features.product.data.irepostory.IProductRepository
import androidtest.features.product.data.repository.ProductRepository
import androidtest.features.product.presenter.viewmodel.ProductViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val productModule = module {

    //Repository
    factory<IProductRepository> {
        ProductRepository(
            dao = get(),
            service = get()
        )
    }

    //ViewModel
    viewModel { ProductViewModel(repository = get()) }

}