package androidtest.features.product.data.repository

import androidtest.features.product.data.irepostory.IProductRepository
import androidtest.libraries.data.dao.ProductDao
import androidtest.libraries.data.entity.asDomainModel
import androidtest.libraries.data.model.Product
import androidtest.libraries.data.model.asEntity
import androidtest.libraries.data.response.DigioProductResponse
import androidtest.libraries.data.service.DigioService
import androidtest.libraries.uicore.exceptions.ListIsEmptyException
import androidtest.libraries.uicore.extensions.resetStatus
import androidtest.libraries.uicore.utils.Result
import androidtest.libraries.uicore.utils.responseError
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidtest.libraries.uicore.exceptions.ResponseNoBodyException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProductRepository(
    val dao: ProductDao,
    val service: DigioService,
) : IProductRepository {

    private val _statusGetListFromServer =
        MutableLiveData<Result<List<Product>>>()
    override val statusGetListFromServer: LiveData<Result<List<Product>>>
        get() = _statusGetListFromServer

    override suspend fun getList() {
        withContext(Dispatchers.IO) {
            val list = getListFromDb()
            if (list.value.isNullOrEmpty())
                getListFromServer()
        }
    }

    override suspend fun getListFromServer() {
        withContext(Dispatchers.IO) {
            _statusGetListFromServer.postValue(Result.InProgress)
            service.getProducts().enqueue(object :
                Callback<DigioProductResponse> {
                override fun onResponse(
                    call: Call<DigioProductResponse>,
                    response: Response<DigioProductResponse>
                ) {
                    try {
                        if (response.isSuccessful) {
                            response.body()?.let {
                                if (it.products.isEmpty()) {
                                    throw ListIsEmptyException()
                                } else {
                                    _statusGetListFromServer.postValue(
                                        Result.Success(it.products)
                                    )
                                }
                            } ?: throw ResponseNoBodyException()
                        } else {
                            _statusGetListFromServer.postValue(
                                responseError(response.code())
                            )
                        }
                    } catch (ex: Exception) {
                        _statusGetListFromServer.postValue(
                            Result.Error(ex, null)
                        )
                    }
                }
                override fun onFailure(call: Call<DigioProductResponse>, t: Throwable) {
                    _statusGetListFromServer.postValue(
                        Result.Error(t, null)
                    )
                }
            })
        }
    }

    override suspend fun createListToDb(list: List<Product>) {
        withContext(Dispatchers.IO) {
            try {
                dao.clearAndInsert(list.asEntity())
            } catch (ex: Exception) {
                ex.toString()
            }
        }
    }

    override fun getListFromDb(): LiveData<List<Product>?> {
        return Transformations.map(dao.getList()) {
            it?.asDomainModel()
        }
    }

    override fun resetStatus(status: IProductRepository.ProductRepositoryStatus) =
        when (status) {
            IProductRepository.ProductRepositoryStatus.GetListFromServer -> _statusGetListFromServer.resetStatus()
        }

}
