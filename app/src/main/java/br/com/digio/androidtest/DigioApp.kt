package br.com.digio.androidtest

import android.app.Application
import androidtest.features.cash.di.cashModule
import androidtest.features.product.di.productModule
import androidtest.features.spotlight.di.spotlightModule
import androidtest.libraries.data.clearDatabase
import androidtest.libraries.data.di.dataModule
import androidtest.libraries.data.getDatabase
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ProcessLifecycleOwner
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class DigioApp : Application(), LifecycleObserver {

    override fun onCreate() {
        super.onCreate()

        var list = mutableListOf(
            dataModule,
            cashModule,
            spotlightModule,
            productModule,
        )
        startKoin {
            androidContext(this@DigioApp)
            modules(list)
        }

        getDatabase(this@DigioApp).clearDatabase()
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
    }

}